import React, { lazy, Suspense } from 'react'
import './App.css'
import Dashboard from './components/dashboard/Dashboard'
import ViewDashboard from './components/dashboard/ViewDashboard'
import { Spin } from 'antd'
import { LoadingOutlined } from '@ant-design/icons'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import Applications from './components/applications/applications'
const Layout = lazy(async () => await import('../src/components/layout/Layout'))
const Page404 = lazy(async () => await import('../src/components/pages/404'))
const antIcon = <LoadingOutlined style={{ fontSize: 24, color: '#37762F' }} spin />

const App: React.FC = () => {

  return (
    <>
      <Router>
        <Suspense fallback={
          <div className='h-full w-full relative align-middle text-center
          justify-center items-center marg'>
            <Spin indicator={antIcon} />
          </div>
        }>
          <Routes>
            <Route element={<Layout />}>
                <Route path="/raw" element={<Dashboard />} />
                <Route path="/view-dashboard" element={<ViewDashboard />} />
                <Route path="/" element={<Applications />} />
                <Route path="*" element={<Page404 />} />
            </Route>
          </Routes>
        </Suspense>
      </Router>
    </>
  )
}

export default App
