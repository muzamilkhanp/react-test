import axios, { type AxiosRequestConfig } from 'axios'

const { REACT_APP_API_BASE_URL } = process.env

const instance: any = axios.create({
  baseURL: REACT_APP_API_BASE_URL ?? 'https://engineering-task.elancoapps.com/api',
  timeout: 500000,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

// Add a request interceptor
instance.interceptors.request.use(
  function (config: AxiosRequestConfig) {
    return { ...config }
  },
  async function (error: unknown) {
    // Do something with request error
    return await Promise.reject(error)
  }
)

const responseBody = (response: Record<string, unknown>): void => {
  const resp: any = response.data
  return resp
}

const requests = {
  get: async (url: string, body?: unknown, headers?: Record<string, unknown>) =>
    instance.get(url, body).then(responseBody),

  post: async (url: string, body: unknown, headers?: Record<string, unknown>) =>
    instance.post(url, body, headers).then(responseBody),

  put: async (url: string, body: unknown, headers?: Record<string, unknown>) =>
    instance.put(url, body, headers).then(responseBody),

  patch: async (url: string, body: unknown) => instance.patch(url, body).then(responseBody),

  delete: async (url: string) => instance.delete(url).then(responseBody)
}

export default requests
