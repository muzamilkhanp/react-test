import requests from './httpService'

const APIService = {
  async getRawData() {
    return await requests.get('/raw')
  },

  async getApplicationList() {
    return await requests.get(`/applications`)
  },

  async getApplicationData(app: any = null) {
    return await requests.get(`/applications/${app ?? null}`)
  }

}

export default APIService
