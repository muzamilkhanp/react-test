import React, { useEffect, useState } from 'react'
import type { TableProps } from 'antd'
import { Card, Form, Select, Table, Spin, message } from 'antd'
import { LoadingOutlined } from '@ant-design/icons'
import type { ColumnsType, SorterResult } from 'antd/es/table/interface'
import { useNavigate } from 'react-router-dom'
import APIService from '../../services/APIService'
import { EyeOutlined } from '@ant-design/icons'

const antIcon = <LoadingOutlined style={{ fontSize: 24, color: '#37762F' }} spin />
const { Option } = Select

interface DataType {
    key: React.Key
    ResourceGroup: string
    ResourceLocation: string
    UnitOfMeasure: string
    ServiceName: string
    Location: string
    Cost: string
}

const Applications: React.FC = () => {
    const navigate = useNavigate()
    const [applicationList, setApplicationList] = useState([])
    const [applicationData, setApplicationData] = useState([])
    const [appType, setAppType] = useState('Delaware-deposit-Plastic')
    const [loading, setLoading] = useState(false)
    const [sortedInfo, setSortedInfo] = useState<SorterResult<DataType>>({})

    useEffect(() => {
        setLoading(true)
        APIService.getApplicationList()
            .then((res: any) => {
                // if (res.status === 200) {
                setApplicationList(res)
                OnApplicationSearch(appType)
                setLoading(false)
                // }
            })
            .catch((err: any) => {
                console.log(err?.message)
                void message.error(err?.message)
                setLoading(false)
            })
    }, [appType])

    const OnApplicationSearch = (event?: any): void => {
        console.log("Application selected Event:", event)
        setAppType(event)
        setLoading(true)
        APIService.getApplicationData(event)
            .then((res: any) => {
                // if (res.status === 200) {
                setApplicationData(res)
                setLoading(false)
                // }
            })
            .catch((err: any) => {
                console.log(err?.message)
                setLoading(false)
            })
    }

    const columns: ColumnsType<DataType> = [
        {
            title: 'Resource Group',
            dataIndex: 'ResourceGroup',
            key: 'ResourceGroup',
            sorter: (a, b) => a.ResourceGroup.localeCompare(b.ResourceGroup),
            sortOrder: sortedInfo.columnKey === 'ResourceGroup' ? sortedInfo.order : null,
            ellipsis: false
        },
        {
            title: 'Resource Location',
            dataIndex: 'ResourceLocation',
            key: 'ResourceLocation',
            sorter: (a, b) => a.ResourceLocation.localeCompare(b.ResourceLocation),
            sortOrder: sortedInfo.columnKey === 'ResourceLocation' ? sortedInfo.order : null,
            ellipsis: false
        },
        {
            title: 'Unit Of Measure',
            dataIndex: 'UnitOfMeasure',
            key: 'UnitOfMeasure',
            sorter: (a, b) => a.UnitOfMeasure.localeCompare(b.UnitOfMeasure),
            sortOrder: sortedInfo.columnKey === 'UnitOfMeasure' ? sortedInfo.order : null,
            ellipsis: false
        },
        {
            title: 'Location',
            dataIndex: 'Location',
            key: 'Location',
            sorter: (a, b) => a.Location.localeCompare(b.Location),
            sortOrder: sortedInfo.columnKey === 'Location' ? sortedInfo.order : null,
            ellipsis: false
        },
        {
            title: 'Cost',
            dataIndex: 'Cost',
            key: 'Cost',
            sorter: (a: any, b: any) => Number(a.Cost) - Number(b.Cost),
            sortOrder: sortedInfo.columnKey === 'Cost' ? sortedInfo.order : null,
            ellipsis: false
        },
        {
            title: 'Action',
            dataIndex: 'action',
            render: (text: any, record: any) => (
                <div className='flex'>
                    <EyeOutlined onClick={() => { navigate('/view-dashboard', { state: record }) }} />
                </div>
            )
        }
    ]

    const data: any = applicationData

    const handleChange: TableProps<DataType>['onChange'] = (pagination, filters, sorter) => {
        setSortedInfo(sorter as SorterResult<DataType>)
    }

    return (
        <div className="w-full">
            <div className="px-5 py-2.5 border-b-slate-200 border border-solid ">
                <div className="flex justify-between items-center">
                    <div className="text-xl font-semibold">Applications</div>
                </div>
            </div>
            <div className="p-7">
                <Card className="bg-white">
                    <div className="pt-2.5">
                        <div className="flex overflow-auto justify-between">
                            <div className="text-xl font-semibold">List of Provided Applications</div>
                            <Form.Item label='Filter' name='appType'>
                                <Select onChange={OnApplicationSearch} placeholder="Filter by application Type"
                                    size="large" defaultValue={'Delaware-deposit-Plastic'} value={appType}>
                                    {applicationList.map((app) => {
                                        return (
                                            <><Option value={app}>{app}</Option></>
                                        )
                                    })}
                                </Select>
                            </Form.Item>
                        </div>
                        <div className="pt-2.5 overflow-auto">
                            {loading
                                ? <div className='text-center justify-center align-middle py-4'>
                                    <Spin indicator={antIcon} /></div>
                                : <Table columns={columns} dataSource={data} onChange={handleChange} />
                            }
                        </div>
                    </div>
                </Card>
            </div>
        </div>
    )
}

export default Applications
