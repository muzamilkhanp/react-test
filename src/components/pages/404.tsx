import React from 'react'
import { Button } from 'antd'
import { useNavigate } from 'react-router-dom'

const NotFound: React.FC = () => {
  const navigate = useNavigate()

  const backToHome = (): void => {
    navigate('/')
  }

  return (
    <>
      <div className="w-full h-screen flex flex-wrap content-center">
        <div className="mx-auto">
          <h2 className="font-bold text-2xl text-gray-700 pb-5">Page is not found!</h2>
          <Button
            onClick={() => {
              backToHome()
            }}
            className="w-full rounded-sm text-sm font-bold text-blue-500"
          >
            Back to Dashboard
          </Button>
        </div>
      </div>
    </>
  )
}

export default NotFound
