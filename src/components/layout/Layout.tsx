import React, { Suspense } from 'react'
import { Outlet } from 'react-router-dom'
import Sidebar from '../../../src/components/layout/Sidebar'
import { Spin } from 'antd'
import { LoadingOutlined } from '@ant-design/icons'
const antIcon = <LoadingOutlined style={{ fontSize: 24, color: '#37762F' }} spin />

const Layout: React.FC<any> = (props: any) => {
  return (
    <div>
      <div className="flex flex-1 w-full">
        <Sidebar />
        <Suspense fallback={
          <div className='h-full w-full relative align-middle text-center
          justify-center items-center marg'>
            <Spin indicator={antIcon} />
          </div>
        }>
          <Outlet />
        </Suspense>
      </div>
    </div>
  )
}

export default Layout
