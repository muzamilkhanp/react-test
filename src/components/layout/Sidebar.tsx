import React from 'react';
import { UploadOutlined, UserOutlined, VideoCameraOutlined } from '@ant-design/icons';
import type { MenuProps } from 'antd'
import { Layout, Menu } from 'antd';
import { useNavigate } from 'react-router-dom'

const { Sider } = Layout
type MenuItem = Required<MenuProps>['items'][number]

const routes: any = {
  1: '/raw',
  2: '/',
  3: '/resources'
}

function getItem(
    label: React.ReactNode,
    key: React.Key,
    icon?: React.ReactNode,
    children?: MenuItem[]
  ): MenuItem {
    return {
      key,
      icon,
      children,
      label
    }
  }

  const system_admin_items: MenuItem[] = [
    getItem('Raw Data', '1', <UserOutlined />),
    getItem('Applications', '2', <UploadOutlined />),
    getItem('Resources', '3', <VideoCameraOutlined />),
  ]

const Sidebar: React.FC = () => {
  const history = useNavigate()

  const pageOpen = (e: any): void => {
      history(routes[e.key])
  }

  return (
    <Layout>
      <Sider
        breakpoint="lg"
        collapsedWidth="0"
        onBreakpoint={(broken) => {
          console.log(broken);
        }}
        onCollapse={(collapsed, type) => {
          console.log(collapsed, type);
        }}
      >
        <div className="logo" />
        <Menu
          onClick={pageOpen}
          theme="dark"
          mode="inline"
          defaultSelectedKeys={['2']}
          items={system_admin_items}
        />
      </Sider>
    </Layout>
  );
};

export default Sidebar;