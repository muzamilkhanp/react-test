import React from 'react'
import { Col, Row } from 'antd'
import { useLocation, useNavigate } from 'react-router-dom'

const ViewDashboard: React.FC = () => {
  const navigate = useNavigate()
  const raw = useLocation().state

  return (
    <div className="w-full">
      <div className="px-5 py-2.5 border-b-slate-200 border border-solid ">
        <div className="flex justify-between items-center">
          <div className="text-xl">
            <span className='cursor-pointer' onClick={() => { navigate('/') }}>
              Application
            </span> {'> '}
            <span className="font-semibold">{raw?.ServiceName}</span>
          </div>
        </div>
      </div>

      <div className='px-7 py-4'>
        <div className="bg-slate-100 p-4">
          <div
            className="green-color font-extrabold text-base flex items-center leading-10">
            Raw details
          </div>
          <Row gutter={[16, 16]}>
            <Col xs={24} xl={8}>
              <div className="text-base font-medium">Service Name</div>
              <div className="text-base font-bold">{raw?.ServiceName}</div>
            </Col>
            <Col xs={24} xl={8}>
              <div className="text-base font-medium">Location</div>
              <div className="text-base font-bold">{raw?.Location}</div>
            </Col>
            <Col xs={24} xl={8}>
              <div className="text-base font-medium">Meter Category</div>
              <div className="text-base font-bold">{raw?.MeterCategory}</div>
            </Col>
          </Row>
          <div className="pt-4">
            <Row gutter={16}>
              <Col xs={24} xl={8}>
                <div className="text-base font-medium">Resource Group</div>
                <div className="text-base font-bold">{raw?.ResourceGroup}</div>
              </Col>
              <Col xs={24} xl={8}>
                <div className="text-base font-medium">Resource Location</div>
                <div className="text-base font-bold">{raw?.ResourceLocation}</div>
              </Col>
              <Col xs={24} xl={8}>
                <div className="text-base font-medium">Unit Of Measure</div>
                <div className="text-base font-bold">{raw?.UnitOfMeasure}</div>
              </Col>
            </Row>
          </div>
          <div className="pt-4">
            <Row gutter={16}>
              <Col xs={24} xl={8}>
                <div className="text-base font-medium">Consumed Quantity</div>
                <div className="text-base font-bold">{raw?.ConsumedQuantity}</div>
              </Col>
              <Col xs={24} xl={8}>
                <div className="text-base font-medium">Cost</div>
                <div className="text-base font-bold">{raw?.Cost}</div>
              </Col>
              <Col xs={24} xl={8}>
                <div className="text-base font-medium">Date</div>
                <div className="text-base font-bold">{raw?.Date}</div>
              </Col>
            </Row>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ViewDashboard
