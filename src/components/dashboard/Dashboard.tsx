import React, { useEffect, useState } from 'react'
import type { TableProps } from 'antd'
import { Card, Table, message, Spin } from 'antd'
import { EyeOutlined } from '@ant-design/icons'
import type { ColumnsType, SorterResult } from 'antd/es/table/interface'
import { useNavigate } from 'react-router-dom'
import { LoadingOutlined } from '@ant-design/icons'
import APIService from '../../services/APIService'
const antIcon = <LoadingOutlined style={{ fontSize: 24, color: '#37762F' }} spin />

interface DataType {
  key: React.Key
  ResourceGroup: string
  ResourceLocation: string
  UnitOfMeasure: string
  ServiceName: string
  Location: string
  Cost: string
}

const UsersAccountsTable: React.FC = () => {
  const navigate = useNavigate()
  const [rawList, setRawList] = useState([])
  const [loading, setLoading] = useState(false)
  const [sortedInfo, setSortedInfo] = useState<SorterResult<DataType>>({})

  useEffect(() => {
    OnUserAccountsSearch()
    // eslint-disable-next-line
  }, [])

  const OnUserAccountsSearch = (): void => {
    setLoading(true)
    APIService.getRawData()
      .then((res: any) => {
        console.log(res)
        // if (res.status === 200) {
          console.log(res?.slice(0, 100))
          setRawList(res?.slice(0, 100))
          setLoading(false)
        // }
      })
      .catch((err: any) => {
        // eslint-disable-next-line
        console.log(err?.message)
        void message.error(err?.message)
        setLoading(false)
      })
  }

  const columns: ColumnsType<DataType> = [
    {
      title: 'Resource Group',
      dataIndex: 'ResourceGroup',
      key: 'ResourceGroup',
      sorter: (a, b) => a.ResourceGroup.localeCompare(b.ResourceGroup),
      sortOrder: sortedInfo.columnKey === 'ResourceGroup' ? sortedInfo.order : null,
      ellipsis: true
    },
    {
      title: 'Resource Location',
      dataIndex: 'ResourceLocation',
      key: 'ResourceLocation',
      sorter: (a, b) => a.ResourceLocation.localeCompare(b.ResourceLocation),
      sortOrder: sortedInfo.columnKey === 'ResourceLocation' ? sortedInfo.order : null,
      ellipsis: true
    },
    {
      title: 'Unit Of Measure',
      dataIndex: 'UnitOfMeasure',
      key: 'UnitOfMeasure',
      sorter: (a, b) => a.UnitOfMeasure.localeCompare(b.UnitOfMeasure),
      sortOrder: sortedInfo.columnKey === 'UnitOfMeasure' ? sortedInfo.order : null,
      ellipsis: true
    },
    {
      title: 'Location',
      dataIndex: 'Location',
      key: 'Location',
      sorter: (a, b) => a.Location.localeCompare(b.Location),
      sortOrder: sortedInfo.columnKey === 'Location' ? sortedInfo.order : null,
      ellipsis: true
    },
    {
      title: 'Cost',
      dataIndex: 'Cost',
      key: 'Cost',
      sorter: (a: any, b: any) => Number(a.Cost) - Number(b.Cost),
      sortOrder: sortedInfo.columnKey === 'Cost' ? sortedInfo.order : null,
      ellipsis: true
    },
    {
      title: 'Action',
      dataIndex: 'action',
      render: (text: any, record: any) => (
        <div className='flex'>
          <EyeOutlined onClick={() => { navigate('/view-dashboard', { state: record }) }} />
        </div>
      )
    }
  ]

  const data: any = rawList
  
  const handleChange: TableProps<DataType>['onChange'] = (pagination, filters, sorter) => {
    setSortedInfo(sorter as SorterResult<DataType>)
  }

  return (
    <div className="w-full">
      <div className="px-5 py-2.5 border-b-slate-200 border border-solid ">
        <div className="flex justify-between items-center">
          <div className="text-xl font-semibold">Raw Data</div>
        </div>
      </div>
      <div className="p-7">
        <Card className="bg-white">
          <div className="pt-2.5">
            <div className="flex justify-between">
              <div className="text-xl font-semibold">List of Raw</div>
            </div>
            <div className="pt-2.5 overflow-auto">
              {loading
                ? <div className='text-center justify-center align-middle py-4'>
                  <Spin indicator={antIcon} /></div>
                : 
                <Table columns={columns} dataSource={data} onChange={handleChange} />
              }
            </div>
          </div>
        </Card>
      </div>
    </div>
  )
}

export default UsersAccountsTable
